package com.testng.demo;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class exceltoDataProvider {

readDatafromExcelfile rde = null;
String projectDir = System.getProperty("user.dir");
String xlFilePath = projectDir + "\\src\\test\\java\\com\\Utility\\TestData.xlsx";
String sheetName = "Credentials";

@Test(dataProvider = "userData")
public void fillUserForm(String userName, String passWord, String dateCreated, String noOfAttempts, String results)
{
System.out.println("UserName:" +userName);	
System.out.println("PassWord:" +passWord);
System.out.println("DateCreated:" +dateCreated);
System.out.println("NoOfAttempts:" +noOfAttempts);
System.out.println("Results:" +results);
System.out.println("*****************************************************************************");
}

@DataProvider(name="userData")
public Object[][] userFormData() throws Exception
{   Object[][] data = testdata(xlFilePath, sheetName);
	return data;
	
}
	
public  Object[][] testdata(String xlFilePath, String sheetName) throws Exception
{   Object[][] excelData= null;
    rde = new readDatafromExcelfile(xlFilePath);
    int rows = rde.getRowCount(sheetName);
    int columns = rde.getColumnCount(sheetName);
    excelData = new Object[rows-1][columns];
    for(int i=1;i<rows;i++)
    {
    	for(int j=0; j<columns; j++)
    	{
    	excelData[i-1][j] = rde.getCellData(sheetName, j, i);
    	}
    }
	
	return excelData;
}



}
