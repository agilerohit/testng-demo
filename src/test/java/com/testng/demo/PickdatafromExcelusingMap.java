package com.testng.demo;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class PickdatafromExcelusingMap {

	static String projectDir = System.getProperty("user.dir");
	public static final String filepath = projectDir + "\\src\\test\\java\\com\\Utility\\Data.xlsx";
	private static File file;
	private static FileInputStream fis;
	private static XSSFWorkbook wb;
	private static XSSFRow row;
	private static XSSFSheet sheet;
	
	 @org.testng.annotations.Test(dataProvider="testdata1")
	 public void Test(Map mapdata) {
	 System.out.println("------Test Started--------------");
	 System.out.println(mapdata.get("baseUrl"));
	 System.out.println(mapdata.get("userName"));
	
	 }
	
	  @DataProvider(name = "testdata1")
	  public Object[][] dataproviderMethod()  throws Exception
	  {
		String projectDir = System.getProperty("user.dir");
		final String filepath = projectDir + "\\src\\test\\java\\com\\Utility\\Data.xlsx";
	 
	  File file = new File(filepath);
	  FileInputStream fis = new FileInputStream(file); 
	  XSSFWorkbook wb = new XSSFWorkbook(fis);
	  XSSFSheet sheet = wb.getSheetAt(0); 
	  wb.close();
	 
	  int rowcount = sheet.getLastRowNum();
	  int colcount = sheet.getRow(0).getLastCellNum();
	  
	  Object[][] obj = new Object[rowcount][1];
	  
	 for (int i = 0; i < rowcount; i++) {
	 Map<Object, Object> datamap = new HashMap<Object, Object>(); 
	 for (int j = 0; j < colcount; j++) {
	  datamap.put(sheet.getRow(0).getCell(j).toString(), sheet.getRow(i + 1).getCell(j).toString()); }
	  obj[i][0] = datamap; } 
	 return obj;
	 
	  }
	 

	//With Out Data Providers
	
	/*public static void loadExcel() throws Exception {
		System.out.println("Loading Excel data...");
		 file = new File(filepath);
	     fis = new FileInputStream(file);
	    wb = new XSSFWorkbook(fis);
		sheet = wb.getSheet("TestData");
		fis.close();

	}

	public static Map<String, Map<String, String>> getDataMap() throws Exception {

		if (sheet == null) {
			loadExcel();
		}

		Map<String, Map<String, String>> superMap = new HashMap<String, Map<String, String>>();
		Map<String, String> myMap = new HashMap<String, String>();

		for (int i = 1; i < sheet.getLastRowNum() + 1; i++) {
			row = sheet.getRow(i);
			String keyCell = row.getCell(0).getStringCellValue();

			int colNum = row.getLastCellNum();
			for (int j = 1; j < colNum; j++) {
				String value = row.getCell(j).getStringCellValue();
				myMap.put(keyCell, value);
			}

			superMap.put("MASTERDATA", myMap);
		}
		return superMap;
	}

	public static String getValue(String key) throws Exception {
		Map<String, String> myVal = getDataMap().get("MASTERDATA");
		String retValue = myVal.get(key);

		return retValue;
	}*/

	public static void main(String[] args) throws Exception {
		//System.out.println(filepath);
		//System.out.println(getValue("baseUrl"));
	}

}
