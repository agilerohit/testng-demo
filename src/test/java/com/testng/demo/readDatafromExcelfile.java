package com.testng.demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.text.Format;



public class readDatafromExcelfile {
	
	public FileInputStream fis = null;	
	public FileOutputStream fos = null;
	public XSSFWorkbook workbook = null;
	public XSSFSheet sheet = null;
	public XSSFRow row = null;
	public XSSFCell cell = null;
	String xlFilePath;
	
	public readDatafromExcelfile(String xlFilePath) throws Exception
	
	{
		this.xlFilePath = xlFilePath;
		fis = new FileInputStream(xlFilePath);
		workbook = new XSSFWorkbook(fis);
		fis.close();
	}
	
	public String getCellData(String sheetName, String colName, int rowNum)
	
	{
		try{
			int Col_Num =-1;
			sheet = workbook.getSheet(sheetName);
			row = sheet.getRow(0);
		
			
		for(int i=0; i<row.getLastCellNum(); i++)
		{
			if (row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
			{
				Col_Num =i;
			}
			
			row = sheet.getRow(rowNum-1);
			cell = row.getCell(Col_Num-1);
			
			if(cell.getCellTypeEnum()==CellType.STRING)
			return cell.getStringCellValue();
		
		
		else if (cell.getCellTypeEnum()==CellType.NUMERIC||cell.getCellTypeEnum()==CellType.FORMULA)
		 {
			String cellValue = String.valueOf(cell.getNumericCellValue());
			
			if (HSSFDateUtil.isCellInternalDateFormatted(cell))
			{
				SimpleDateFormat  df = new SimpleDateFormat("dd/MM/yy");
				Date date = cell.getDateCellValue();
				cellValue = df.format(date);
			}
			
			return cellValue;
		}
			
		else if (cell.getCellTypeEnum()==CellType.BLANK)
		return "";
			
		else 
		return String.valueOf(cell.getBooleanCellValue());
		
	}
		}
		
		catch (Exception e)
		{
		e.printStackTrace();
		
		return "row "+ rowNum+" or column "+colName+" does not exist in Excel";
		}
		return colName;
		}
		
		public String getCellData(String sheetName, int colNum, int rowNum)
		{
			try {
				 sheet = workbook.getSheet(sheetName);
				 row = sheet.getRow(rowNum);
				 cell = row.getCell(colNum);
				 
				 if (cell.getCellTypeEnum()==CellType.STRING)
					 return cell.getStringCellValue();
				 
				 else if (cell.getCellTypeEnum()==CellType.NUMERIC || cell.getCellTypeEnum()==CellType.FORMULA)
				 {
					 String CellValue = String.valueOf(cell.getNumericCellValue());
					 
					 if(HSSFDateUtil.isCellDateFormatted(cell))
					 {
						 SimpleDateFormat  df = new SimpleDateFormat("dd/MM/yy");
						 Date date = cell.getDateCellValue();
						 CellValue = df.format(date);
					 }
					 return CellValue;
				 }
				 
				 else if (cell.getCellTypeEnum()==CellType.BLANK)
					 return "";
				 
				 else 
				 return String.valueOf(cell.getBooleanCellValue())	; 
				
			}
			
			catch (Exception e)
			
			{
				e.printStackTrace();
				return "row "+ rowNum+" or column "+colNum+" does not exist in Excel";
			}
		}
		
		public boolean setCellData(String sheetName, int colNumber, int rolNum, String value)
		{
			try
			{
				sheet = workbook.getSheet(sheetName);
				row = sheet.getRow(rolNum);
				
				if(row==null)
					row = sheet.createRow(rolNum);
				cell = row.getCell(colNumber);
				
				if (cell==null)
					cell = row.createCell(colNumber);
				cell.setCellValue(value);
				
				fos = new FileOutputStream(xlFilePath);
				workbook.write(fos);
				fos.close();
			}
			
			catch(Exception ex)
			{
				ex.printStackTrace();
				return false;
			}
			return true;
			}
		
		public int getRowCount(String sheetName)
		{
			sheet = workbook.getSheet(sheetName);
			int rowCount = sheet.getLastRowNum()+1;
			return rowCount;
		}
		
		public int getColumnCount(String sheetName)
		{
			sheet = workbook.getSheet(sheetName);
		    row = sheet.getRow(0);
		    int colCount = row.getLastCellNum();
		    return colCount;
		}
		}
	


