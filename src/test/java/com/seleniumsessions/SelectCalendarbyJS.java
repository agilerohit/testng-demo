package com.seleniumsessions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelectCalendarbyJS {
    static WebDriver driver;
	public static void main(String[] args) {
	
		 System.setProperty("webdriver.chrome.driver", "F:/testng-demo/src/test/java/com/Utility/chromedriver.exe");
		  driver = new ChromeDriver();
		  driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  driver.manage().deleteAllCookies();
		  driver.manage().window().maximize();
		  driver.get("https://spicejet.com");	
		  
		  WebElement date = driver.findElement(By.id(""));
		  String dateVal = "31-12-2017";
		  selectDateByJS(driver, date, dateVal);

	}
	
	public static void selectDateByJS(WebDriver driver, WebElement element, String dateVal)
	{
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("argument[0].setAttribute('value','"+dateVal+"');", element);
	}

}
