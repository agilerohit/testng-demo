package com.seleniumsessions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Framehandling {
	
static WebDriver driver;	
	
public static void main(String[]args) throws Exception

{      String dir = System.getProperty("user.dir");
       String path = dir + "/src/test/java/com/Utility/chromedriver.exe";
       System.out.println(path);
       System.setProperty("webdriver.chrome.driver", path);
	  //System.setProperty("webdriver.chrome.driver", "F:/testng-demo/src/test/java/com/Utility/chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
	  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	  driver.manage().deleteAllCookies();
	  driver.manage().window().maximize();
	  driver.get("https://www.freecrm.com/index.html");
	  Thread.sleep(1000);
	  driver.findElement(By.xpath("//input[@type='text']")).sendKeys("rohittest");
	  driver.findElement(By.xpath("//input[@type='password']")).sendKeys("rohittest");
	  driver.switchTo().frame("intercom-borderless-frame");
	  Actions action = new Actions(driver);
	  action.moveToElement(driver.findElement(By.xpath("//div[contains(@class,'intercom-chat-card-author')]"))).build().perform();
	  driver.findElement(By.xpath("//div[contains(@class,'intercom-borderless-dismiss-button')]//span")).click();
	  Thread.sleep(1000);
	  driver.findElement(By.xpath("//input[@type='submit']")).click();
	  driver.switchTo().frame("mainpanel");
	  Thread.sleep(1000);
	  driver.findElement(By.xpath("//a[contains(text(),'Contacts')]")).click();
	  driver.quit();
}

}
