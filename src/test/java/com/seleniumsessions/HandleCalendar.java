package com.seleniumsessions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class HandleCalendar {
	
	static WebDriver driver;

	public static void main(String[] args) throws Throwable {
		
		  System.setProperty("webdriver.chrome.driver", "F:/testng-demo/src/test/java/com/Utility/chromedriver.exe");
		  driver = new ChromeDriver();
		  driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  driver.manage().deleteAllCookies();
		  driver.manage().window().maximize();
		  driver.get("https://www.freecrm.com");
		  Thread.sleep(1000);
		  System.out.println(driver.getTitle());
		  driver.findElement(By.xpath("//input[@type='text']")).sendKeys("rohittest");
		  driver.findElement(By.xpath("//input[@type='password']")).sendKeys("rohittest");
		  driver.switchTo().frame("intercom-borderless-frame");
		  Actions action = new Actions(driver);
		  action.moveToElement(driver.findElement(By.xpath("//div[contains(@class,'intercom-chat-card-author')]"))).build().perform();
		  driver.findElement(By.xpath("//div[contains(@class,'intercom-borderless-dismiss-button')]//span")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//input[@value='Login' and @type='submit']")).click();
		  driver.switchTo().frame("mainpanel");
		  
		  String date = "18-September-2017";
		  String[]dateArray = date.split("-");
		  String day = dateArray[0];
		  String month = dateArray[1];
		  String year = dateArray[2];
		  
		  Select select = new Select(driver.findElement(By.name("slctMonth")));
		  select.selectByVisibleText(month);
		  
		  Select select1 = new Select(driver.findElement(By.name("slctYear")));
		  select1.selectByVisibleText(year);
		  
		  String beforeXpath = "//*[@id='crmcalendar']/table/tbody/tr[2]/td/table/tbody/tr[";
		  String afterXpath = "]/td[";
		  final int totalWeekDays = 7;
		  
		  boolean flag = false;
		  String dayVal = null;
		  for(int rowNum=2; rowNum<=7; rowNum++)
		  {
			  for(int columnNum=1; columnNum<=totalWeekDays; columnNum++)  {
			  try{
				  dayVal = driver.findElement(By.xpath(beforeXpath+rowNum+afterXpath+"]")).getText();
			  }
			  
			  catch(NoSuchElementException e)
			  {
				  System.out.println("Please enter a correct value");
				  flag =false;
				  break;
			  }
			  
			  System.out.println(dayVal);
			  if(dayVal.equals(day)){
			  driver.findElement(By.xpath(beforeXpath+rowNum+afterXpath+"]")).click(); 
			  flag = true;
			  break;
			  
			  }
			  }
			  
			  if(flag){
			  break;
			  }
		  }
	
	

	}
}

