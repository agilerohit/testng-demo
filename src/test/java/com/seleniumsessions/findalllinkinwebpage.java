package com.seleniumsessions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class findalllinkinwebpage {
	static WebDriver driver;

	public static void main(String[] args) {

		//System.setProperty("webdriver.chrome.driver", "F:/testng-demo/src/test/java/com/Utility/chromedriver.exe");
		//driver = new ChromeDriver();
		WebDriverManager.chromedriver().version("2.38").setup();
		driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("https://www.freecrm.com");
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int size = links.size();

		System.out.println("Total number of page on the webpage:" + size);
		String[] texts = new String[size];
		int t = 0;
		for (WebElement text : links) {
			texts[t] = text.getText();// extract text from link and put in Array
			// System.out.println(texts[t]);
			t++;
		}

		for (String clicks : texts) {

			driver.findElement(By.linkText(clicks)).click();
			if (driver.getTitle().equals("notWorkingUrlTitle")) {
				System.out.println("\"" + t + "\"" + " is not working.");
			} else {
				System.out.println("\"" + t + "\"" + " is working.");
			}

			driver.navigate().back();
		}

	}

}
