package JavaPractice;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class countelementFrequency {

	public static void main(String[] args) {
	
	List<String> list = new ArrayList<String>();
	list.add("a");
	list.add("b");
	list.add("a");
	list.add("e");
	list.add("a");
	list.add("a");
	list.add("z");
	System.out.println("a:" + Collections.frequency(list, "a"));
	
	//Print frequency for all element
	Set<String> unique = new HashSet<String>(list);
	for(String temp :unique)
	{
	System.out.println(temp + ": " + Collections.frequency(list, temp));	
	}

	}

}
