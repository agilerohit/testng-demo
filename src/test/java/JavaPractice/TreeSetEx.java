package JavaPractice;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetEx {

	public static void main(String[] args) {
	TreeSet t = new TreeSet(new MyComp());
	t.add(new StringBuffer("rohit"));
	t.add("raman");
	t.add(new StringBuffer("rohit1"));
	t.add("raman1");
	System.out.println(t);
	}
}
	class MyComp implements Comparator
	{
		

		public int compare(Object o1 , Object o2) {
		String s1 = o1.toString();
		String s2 = o2.toString();
		return s1.compareTo(s2);
		}
		
	}


