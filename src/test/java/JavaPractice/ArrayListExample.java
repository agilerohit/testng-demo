package JavaPractice;

import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {
		Emp e1 = new Emp(111, "rohit");
		Emp e2 = new Emp(112, "rohit1");
		Student s = new Student(121, "test");
		
		ArrayList<Emp> al = new ArrayList<Emp>();
		al.add(e1);
		al.add(e2);
		for(Emp e:al)
		{
		System.out.println(e.eid +"-----------"+ e.ename);
		}
		
		System.out.println(al.isEmpty());
		al.remove(e2);
		for(Emp e:al)
		{
		System.out.println(e.eid +"-----------"+ e.ename);
		}

	}

}
