package JavaPractice;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetEx {

	public static void main(String[] args) {
	
	LinkedHashSet<String> h = new LinkedHashSet<String>();	
	h.add("A");
	h.add("B");
	h.add("C");
	h.add("A");
	h.add(null);
	h.add(null);
	
	Iterator<String> itr = h.iterator();
	while(itr.hasNext())
	{
	String str = itr.next();
	System.out.println(str);
	}
	}

}
