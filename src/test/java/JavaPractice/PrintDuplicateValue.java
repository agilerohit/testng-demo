package JavaPractice;
import java.util.Arrays;
import java.util.List;

public class PrintDuplicateValue {

	public static void main(String[] args) {
		int[] strArray = { 2, 2, 4, 7, 7, 9, 11, 11, 11, 14, 16, 18, 18, 18, 18 };

		// First way
		for (int i = 0; i < strArray.length - 1; i++) {
			int count = 0;

			for (int j = i + 1; j <= strArray.length - 1; j++) {
				if ((strArray[i] == (strArray[j]))) {
					count++;

				}

			}
			if (count == 1) {
			System.out.print(" " + strArray[i]);

			}

		}
	}
	
		/*//Second way
				List<int[]> hs = Arrays.asList(strArray);
											
				for (int arrayElement : strArray) {
					//if(!hs.add(arrayElement)){
						
					}
					if (!hs.add(arrayElement)) {
						System.out.println("Duplicate Element is : " + arrayElement);
					}
				}*/
	
	}
//}
