package JavaPractice;

import java.util.ArrayList;
import java.util.Collections;

public class Sorting {

	public static void main(String[] args)
	{
	//Sorting Using Emp id and Emp name
	 ArrayList<Emp> al = new ArrayList<Emp>();
	 al.add(new Emp(111, "ram"));
	 al.add(new Emp(230, "rohan"));
	 al.add(new Emp(119, "shivay"));
	 al.add(new Emp(102, "Arman"));
	 Collections.sort(al);
	 for(Emp e:al)
	 {
	 System.out.println(e.eid + "------" +e.ename);	 
	 }
	}

}
