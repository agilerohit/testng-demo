package JavaPractice;

public class LinkedList {
	ListNode head;
	int length;

	public void insertnodeaAtBegin(int data) {
		ListNode node = new ListNode(data);
		node.setNext(head);
		head = node;
		length++;
	}

	public void insertnodeAtEnd(int data) {
		ListNode node = new ListNode(data);
		ListNode temp1, temp2;
		if (head == null) {
			head = node;
		} else {
			for (temp1 = head; (temp2 = temp1.getNext()) != null; temp1 = temp2) {

			}
			temp1.setNext(node);
		}

		length++;
	}

	public void insertBasedOnIndex(int data, int position) {
		ListNode temp = head;
		for (int i = 1; i < position; i++) {
			temp = temp.getNext();
		}

		ListNode node = new ListNode(data);
		node.next = temp.getNext();
		temp.setNext(node);
		length++;

	}

	public void removefromNode() {
		ListNode node = head;
		if (node != null) {
			head = head.getNext();
			node.setNext(null);
		}
	}

	public void deleteNodeFromEnd() {
		ListNode temp1, temp2;
		for (temp1 = head; (temp1.getNext().getNext()) != null; temp1 = temp1.getNext()) {

		}
		temp1.setNext(null);
	}

	public void deleteNodeBasedonPosition(int position) {
		ListNode temp = head;
		for (int i = 1; i < position; i++) {
			temp = temp.getNext();
		}

		temp.setNext(temp.getNext().getNext());
	}

	public static void main(String[] args) {

		LinkedList obj = new LinkedList();
		LinkedList obj1 = new LinkedList();
		obj.insertnodeaAtBegin(6);
		obj.insertnodeaAtBegin(7);
		System.out.println(obj.toString());
		obj1.insertnodeAtEnd(12);
		obj1.insertnodeAtEnd(14);
		obj1.insertnodeAtEnd(17);
		System.out.println(obj1.toString());

	}

	public String toString() {
		String data = "[";
		data = data + head.getData();
		ListNode temp = head.getNext();

		while (temp != null) {
			data = data + "----------------->" + temp.getData();
			temp = temp.getNext();
		}

		return data + "]";
	}

}
