package JavaPractice;
import java.util.ArrayList;
import java.util.Arrays;

public class convertarrayintoarraylist {

	public static void main(String[] args) {
	    //conversion of array into arraylist
		String[] s = {"rohit", "ratan", "anu", "ajay"};
		ArrayList<String> al = new ArrayList<String>(Arrays.asList(s));
		al.add("aaa");
		al.add("bbb");
		System.out.println(al);
		
		//conversion of collection into array
		String[]s2 = new String[al.size()];  
		al.toArray(s2);
		for(String str:al)
		{
		System.out.println(str);
		}	
		
		//Normal version above one was generic
		ArrayList a2 = new ArrayList();
		a2.add(10);
		a2.add("rohit2");
		a2.add(10.5);
		a2.add(true);
		
		Object[] oo = a2.toArray();
		for(Object o :oo)
		{
		System.out.println(o);	
		}
		

	}

}
