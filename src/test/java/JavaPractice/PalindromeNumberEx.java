package JavaPractice;

public class PalindromeNumberEx {

	public static void main(String[] args) {
	
		int no = 575;
		int sum = 0;
		int temp = no;

		while (no > 0) {
			int remainder = no % 10;
			sum = sum * 10 + remainder;
			no = no/10;
		}
		if (temp == sum) {
			System.out.println("No is palindrome");
		}

		else {
			System.out.println("No is not palindrome");
		}

	}

}
